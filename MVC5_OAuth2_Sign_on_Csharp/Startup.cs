﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MVC5_OAuth2_Sign_on_Csharp.Startup))]
namespace MVC5_OAuth2_Sign_on_Csharp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
